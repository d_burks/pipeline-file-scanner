#!/mnt/clinical/bin/miniconda3/bin/python
'''
Pipeline file monitoring script.  Initial list of pipeline-related files scraped
from bitbucket repo and pipeline log files in June 2021. 

Default cutoffs for file-size and age are 1024 bytes and 5 hours.  Pipeline files 
that are identified as younger than 5 hours and less than or equal to 1024 bytes 
will generate an alert.  As this script is meant to run on schedule (e.g. crontab),
please ensure that the schedule is less than the pipeline file age cutoff.
'''

from pathlib import Path
import os
import time

timestamp = time.strftime('%l:%M%p %Z - %b %d, %Y')

warnfile = open('/NGSData/Burks/Pipeline_Scanner/warnings.txt','a')
alertfile = open('/NGSData/Burks/Pipeline_Scanner/alerts.txt','a')
logfile = open('/NGSData/Burks/Pipeline_Scanner/full_log.txt','a')
missingfiles = open('/NGSData/Burks/Pipeline_Scanner/missing.txt','a')

pipefiles = []
with open('/mnt/dburks/Scripts/Log_Scanner/Scanner/file_list.txt') as infile:
    for lines in infile:
        lines = lines.rstrip()
        if Path.is_file(Path(lines)):
            pipefiles.append(lines)
        else:
            missingfiles.write(f'{timestamp} : {lines} no longer exists!')
            
#Scan file list and check for last modified date.
def file_date_check(pfile,hours=5):
    pfile_time = (Path(pfile)).stat().st_mtime
    return ((time.time() - pfile_time) / 3600 < 1*hours)
    
def file_size_check(pfile,size=1024):
    pfile_size = (Path(pfile)).stat().st_size
    return (pfile_size < size)
    
bads = []
smalls = []
news = []

for p in pipefiles:
    if (file_date_check(p) and file_size_check(p)):
        lout = (os.popen(f'ls -lhat {p}').read()).rstrip()
        bads.append(lout)
    elif file_date_check(p):
        lout = (os.popen(f'ls -lhat {p}').read()).rstrip()
        news.append(lout)
    elif file_size_check(p):
        lout = (os.popen(f'ls -lhat {p}').read()).rstrip()
        smalls.append(lout)
        
if len(bads) > 0:
    alertfile.write('Files less than 5 hours old and 1kb in size ')
    alertfile.write(timestamp + '\n')
    for b in bads:
        alertfile.write(b + '\n')
    alertfile.write('\n')
        
if len(smalls) > 0:
    warnfile.write('Files less than 1kb in size ')
    warnfile.write(timestamp + '\n')
    for s in smalls:
        warnfile.write(s + '\n')
    warnfile.write('\n')
        
if len(news) > 0:
    warnfile.write('Files less than 5 hours old ')
    warnfile.write(timestamp + '\n')
    for n in news:
        warnfile.write(n + '\n')
    warnfile.write('\n')
    
logfile.write(f'File status as of {timestamp}\n')
for p in pipefiles:
    lout = (os.popen(f'ls -lhat {p}').read()).rstrip()
    logfile.write(lout + '\n')
logfile.write('\n')

for ff in [warnfile,alertfile,logfile,missingfiles]:
    ff.close()
