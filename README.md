# Pipeline Scanner Tool

### python3 pipeline_file_scan.py

This will scan all files in file_list.txt and appends to 4 log files:

1. Warnings (warnfile) - Files that are less than 1kb in size or less than 5 hours old.
2. Alerts (alertfile) - Files that are less than 1kb in size AND less than 5 hours old.
3. Log (logfile) - Status of all files at time of script run.
4. Missing (missingfile) - Files in file_list.txt that are no longer available.
